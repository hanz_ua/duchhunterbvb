package com.bvblogic.duckhunterbvb;

import android.os.Handler;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bvblogic.duckhunterbvb.controller.AnimateDogController;
import com.bvblogic.duckhunterbvb.controller.AnimateDuckController;
import com.bvblogic.duckhunterbvb.controller.AnimatorListener;
import com.bvblogic.duckhunterbvb.controller.core.EAnimal;
import com.bvblogic.duckhunterbvb.controller.core.SoundBean;

import java.util.Random;

public class MainActivity extends CoreActivity implements AnimatorListener.AnimationEnd, View.OnClickListener {
    private ImageView dog;
    private ImageView duck;
    private RelativeLayout duckContainer;
    private RelativeLayout gameContainer;
    private TextView countScore;
    private int count = 0;
    private int life = 0;
    private LinearLayout heartСontainer;
    private ImageView heart1, heart2, heart3;
    AnimatorListener.AnimationEnd animationEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        heart1 = findViewById(R.id.heart1);
        heart2 = findViewById(R.id.heart2);
        heart3 = findViewById(R.id.heart3);

        heartСontainer = findViewById(R.id.heart_container);
        gameContainer = findViewById(R.id.game_container);
        dog = findViewById(R.id.dog);
        duck = findViewById(R.id.duck);
        duckContainer = findViewById(R.id.container_ducks);
        countScore = findViewById(R.id.count_score);
        countScore.setText(String.valueOf(count));
        createDuck();
        AnimateDogController.setAnimationEnd(this);
        setListener();


        new Handler().postDelayed(() -> AnimateDogController.showDog(dog), 1000);

    }

    private ImageView createDuck() {
        ImageView imgDuck = new ImageView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.bottomMargin = 0;
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        params.addRule(RelativeLayout.ABOVE, R.id.grass);
        imgDuck.setLayoutParams(new RelativeLayout.LayoutParams(params));
        imgDuck.setOnClickListener(v -> {
            count++;
            countScore.setText(String.valueOf(count));
            new SoundBean(this).playBeepSoundAndVibrate(R.raw.gunshot);
            imgDuck.setVisibility(View.GONE);
        });
        gameContainer.addView(imgDuck);
        return imgDuck;
    }

    private void setListener() {
        gameContainer.setOnClickListener(this);
    }

    private void createDuckHandler(){
        Random random = new Random();

        new Handler().post(() ->new AnimateDuckController().showDuck(gameContainer, createDuck(), duckContainer, this));

         new Handler().postDelayed(this::createDuckHandler, random.nextInt(500) + 1000);
    }


    @Override
    public void onAnimationEnd(EAnimal animator) {
        switch (animator) {
            case DOG:
                dog.setVisibility(View.GONE);
                createDuckHandler();
            case DUCK:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.game_container:
                life++;
                new SoundBean(this).playBeepSoundAndVibrate(R.raw.gunshot);
                deadHunter(life);
                break;
        }
    }


    private void deadHunter(int l) {
        switch (l) {
            case 1:
                heart1.setVisibility(View.GONE);
                break;
            case 2:
                heart2.setVisibility(View.GONE);
                break;
            case 3:
                heart3.setVisibility(View.GONE);
                Toast.makeText(this, "Ви програли", Toast.LENGTH_SHORT).show();
                onBackPressed();
                break;
        }
    }
}
