package com.bvblogic.duckhunterbvb.controller;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;

import com.bvblogic.duckhunterbvb.controller.core.EAnimal;
import com.bvblogic.duckhunterbvb.controller.core.Utils;

public class AnimateDogController {
private static AnimatorListener.AnimationEnd animationEnd;
    public static void showDog(final ImageView dog) {
        float first = dog.getY();
        dog.setY((first + Utils.dpToPx(150)));

        ObjectAnimator anim = ObjectAnimator.ofFloat(dog, "y", first);
        anim.setDuration(3000);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                dog.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animationEnd.onAnimationEnd(EAnimal.DOG);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        anim.start();

    }

    public static void setAnimationEnd(AnimatorListener.AnimationEnd animationEnd) {
        AnimateDogController.animationEnd = animationEnd;
    }
}
