package com.bvblogic.duckhunterbvb.controller;

import android.animation.Animator;
import android.view.View;

import com.bvblogic.duckhunterbvb.controller.core.EAnimal;

public interface AnimatorListener {
    interface AnimationStart {
        void onAnimationStart(EAnimal animator);
    }

    interface AnimationEnd {
        void onAnimationEnd(EAnimal animator);
    }

    interface AnimationCancel {
        void onAnimationCancel(EAnimal animator);
    }

    interface AnimationRepeat {
        void onAnimationRepeat(EAnimal animator);
    }
}
