package com.bvblogic.duckhunterbvb.controller.core;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.annotation.RawRes;
import android.util.Log;

import com.bvblogic.duckhunterbvb.MainActivity;

import java.io.IOException;


public class SoundBean {


    private static final float BEEP_VOLUME = 0.10f;

    public MainActivity context;

    private boolean beepEnabled = true;
    private boolean vibrateEnabled = false;

    public SoundBean(MainActivity context) {
        this.context = context;
        context.setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    public boolean isBeepEnabled() {
        return beepEnabled;
    }


    public synchronized void playBeepSoundAndVibrate(@RawRes int id) {
        if (beepEnabled) {
            playBeepSound(id);
        }
    }


    private void playBeepSound(@RawRes int id) {
        MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        mediaPlayer.setOnCompletionListener(mp -> {
            mp.stop();
            mp.release();
        });
        mediaPlayer.setOnErrorListener((mp, what, extra) -> {
            // possibly media player error, so release and recreate
            mp.stop();
            mp.release();
            return true;
        });
        try {
            try (AssetFileDescriptor file = context.getResources().openRawResourceFd(id)) {
                mediaPlayer.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
            }
            mediaPlayer.setVolume(BEEP_VOLUME, BEEP_VOLUME);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException ioe) {
            mediaPlayer.release();
        }
    }

}
