package com.bvblogic.duckhunterbvb.controller;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bvblogic.duckhunterbvb.R;

import java.util.ArrayList;

public class AnimateDuckController {

    private  AnimatorSet animatorSet;

    public void showDuck(RelativeLayout gameContainer, final ImageView duck, final RelativeLayout container, Activity baseContext) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) baseContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        duck.setY(1000);
        final float firstY = 1000;
        final float firstX = duck.getX();

        int randomPoint = random(duck);
        ObjectAnimator animY = ObjectAnimator.ofFloat(duck, "y", 0 - 200);

        ObjectAnimator animX = ObjectAnimator.ofFloat(duck, "x", randomPoint);
        animatorSet = new AnimatorSet();
        animatorSet.playTogether(animY, animX);
        animatorSet.setDuration(3000);
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                duck.setVisibility(View.VISIBLE);
                AnimatedDuckFlyController.showFlyDuck(duck, AnimatedDuckFlyController.listDR = locationFlyDuck(randomPoint,baseContext));
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                duck.setY(firstY);
                duck.setX(getRandomPointStartDuck(container));
                AnimatedDuckFlyController.stopFlyDuck();
                AnimatedDuckFlyController.listDR.clear();
                gameContainer.removeView(duck);
               // showDuck(duck,container, baseContext);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.start();
    }

    private  int random(ImageView duck){
        int i = (int) (Math.random() * +1920);
        duck.setX(i);
       return i;
    }

    private  int getMidlFLy(){
        return Resources.getSystem().getDisplayMetrics().heightPixels/2;
    }
    private  int getRandomPointStartDuck(final RelativeLayout container){
        int max = container.getWidth();
        int min = (int) container.getX();
        return (int) (Math.random() * ++max) + min ;
    }

    private  ArrayList<Drawable> locationFlyDuck(int x, Context context){
        ArrayList<Drawable> anim = new ArrayList<>();
        anim.clear();
        if(x>0&&x<1920/3){
            anim.add(context.getResources().getDrawable(R.mipmap.right_duck_1));
            anim.add(context.getResources().getDrawable(R.mipmap.right_duck_2));
            anim.add(context.getResources().getDrawable(R.mipmap.right_duck_3));

            return anim;
        }
        else if(x<1920*2/3&&x>1920/3) {
            anim.add(context.getResources().getDrawable(R.mipmap.up_duck_1));
            anim.add(context.getResources().getDrawable(R.mipmap.up_duck_2));
            anim.add(context.getResources().getDrawable(R.mipmap.up_duck_3));
            return anim;
        }
        else if(x<1920&&x>1920*2/3){

            anim.add(context.getResources().getDrawable(R.mipmap.left_duck_1));
            anim.add(context.getResources().getDrawable(R.mipmap.left_duck_2));
            anim.add(context.getResources().getDrawable(R.mipmap.left_duck_3));
            return anim;
        }
        else {
            return new ArrayList<>();
        }
    }

}
