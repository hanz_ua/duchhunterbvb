package com.bvblogic.duckhunterbvb.controller;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

import com.bvblogic.duckhunterbvb.R;

import java.nio.file.attribute.AclEntry;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class AnimatedDuckFlyController {
    private static final int ANIM_DUCK = 0;
    private ImageView imgDuck;
    private static Disposable imgDisposable;
    private static Disposable listDisposable;
    public static ArrayList<Drawable> listDR;

    static void showFlyDuck(final ImageView duck, ArrayList<Drawable> list) {
        listDR = list;
        stopFlyDuck();
        listDisposable = Observable.just(list).subscribeOn(Schedulers.io()).subscribe(list1 -> {
            for (Drawable d :
                    list) {
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                imgDisposable = Observable.just(d).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(duck::setImageDrawable, throwable -> {
                }, () -> imgDisposable.dispose());
            }
        }, throwable -> {
        }, () -> showFlyDuck(duck, listDR));
    }

    static void stopFlyDuck() {
        if (imgDisposable != null && imgDisposable.isDisposed()) imgDisposable.dispose();
        if (listDisposable != null && listDisposable.isDisposed()) listDisposable.dispose();
    }


}
